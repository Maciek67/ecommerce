import styled from "styled-components";
import { mobile } from "../responsive";
import { useState } from "react";
import { login } from "../redux/apiCalls";
import { useDispatch, useSelector } from "react-redux";



const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background-image: linear-gradient(rgba(255, 255, 255, 0.5), 
  rgba(255, 255, 255, 0.5)), 
  url("https://cdn.pixabay.com/photo/2019/03/05/18/06/tirol-4036725__340.jpg");
  display: flex;
  align-items: center;
  justify-content: center;
  background-repeat: no-repeat;
  background-size: cover;
  
  `;
const Wrapper = styled.div`
  width: 25%;
  padding: 20px;
  background-color: #fff;
  ${mobile({ width: "80%"})};

`;
const Form = styled.form`
  display: flex;
  flex-direction: column;

`;
const Title = styled.h1`
  font-size: 24px;
  font-weight: 300;
`;
const Input = styled.input`
  flex: 1;
  min-width: 40%;
  margin: 10px  0px;
  padding: 10px;
`;

const Button = styled.button`
  width: 40%;
  border: none;
  padding: 15px 20px;
  background-color: teal;
  color: white;
  cursor: pointer;
  margin-bottom: 10px;
  &:disabled {
    color: green;
    cursor: not-allowed;
  };
`;

const Link = styled.a`
  margin: 5px 0px;
  font-size: 12px;
  text-decoration: underline;
  cursor: pointer;
`;

const Error = styled.span`
  color: red;
`


const Login = () => {

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const { isFetching, error } = useSelector((state)=> state.user);


  const handleClick = (e) => {
    e.preventDefault();
    login(dispatch, { username, password });
  };

  return (
    <Container>
       <Wrapper>
        <Title>ZALOGUJ SIĘ</Title>
        <Form>
          <Input placeholder="Nazwa użytkownika" onChange = {(e)=> setUsername(e.target.value)} />
          <Input placeholder="Hasło" type="password" onChange={(e) => setPassword(e.target.value)} />
          <Button onClick={handleClick} disabled={isFetching} >ZALOGUJ</Button>
          { error &&  <Error>Coś poszło nie tak...</Error>}
          <Link>NIE PAMIĘTASZ HASŁA ?</Link>
          <Link>UTWÓRZ NOWE KONTO</Link>
        </Form>
      </Wrapper>


    </Container>
  );
};

export default Login;