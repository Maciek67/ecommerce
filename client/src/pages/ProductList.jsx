import styled from "styled-components";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import Newsletter from "../components/Newsletter";
import Products from "../components/Products";
import { mobile, tablet } from "../responsive";
import { useLocation } from "react-router";
import { useState } from "react";



const Container = styled.div``;
const Title = styled.h1`
  margin: 20px;
`;
const FilterContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;
const Filter = styled.div`
  margin: 20px;
  ${mobile({ width: "0px 20px", display: "flex", flexDirection: "column"})};
  ${tablet({ width: "0px 20px", display: "flex", flexDirection: "column"})};
`;

const FilterText = styled.span`
  font-size: 20px;
  font-weight: 600;
  margin-right: 20px;
  ${mobile({ marginRight: "0px"})};
`;

const Select = styled.select`
  padding: 10px;
  margin-right: 20px;
  ${mobile({ margin: "10px 0px"})};
`;

const Option = styled.option``;



const ProductList = () => {
  const location = useLocation();
  const cat = location.pathname.split("/")[2];
  const [filters, setFilters] = useState({});
  const [sort, setSort] = useState("newest");

  const handleFilters = (e) => {
    const value = e.target.value;
    setFilters({
      ...filters,
      [e.target.name]: value,
    });
  };



  return (
    <Container>
      <Navbar />
    <Title>Zestawy narciarskie - {decodeURIComponent(cat)}</Title>
      <FilterContainer>
        <Filter>
          <FilterText>Filtrowanie produktów</FilterText>
          <Select name="color" onChange={handleFilters}>
            <Option disabled >Kolor</Option>
            <Option>Biały</Option>
            <Option>Czarny</Option>
            <Option>Czerwony</Option>
            <Option>Niebieski</Option>
            <Option>Żółty</Option>
            <Option>Zielony</Option>
          </Select>
          <Select name="size" onChange={handleFilters}>
            <Option disabled >Długość</Option>
            <Option>150cm</Option>
            <Option>155cm</Option>
            <Option>160cm</Option>
            <Option>165cm</Option>
            <Option>170cm</Option>
            <Option>175cm</Option>
            <Option>180cm</Option>
            <Option>185cm</Option>
          </Select>
        </Filter>
        <Filter>
          <FilterText>Sortowanie produktów</FilterText>
          <Select onChange={(e)=> setSort(e.target.value)}>
            <Option value="newest">Najnowsze</Option>
            <Option value="asc">Cena rosnąco</Option>
            <Option value="dsc">Cena malejąco</Option>
          </Select>
        </Filter>
      </FilterContainer>
      <Products cat={cat} filters={filters} sort={sort} />
      <Newsletter />
      <Footer />
    </Container>
  );
};

export default ProductList;
