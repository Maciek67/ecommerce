import styled from "styled-components";
import { mobile } from "../responsive";


const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background-image: linear-gradient(rgba(255, 255, 255, 0.5),
  rgba(255, 255, 255, 0.5)),
  url("https://cdn.pixabay.com/photo/2019/03/05/18/06/tirol-4036725__340.jpg");
  display: flex;
  background-size: cover;
  align-items: center;
  justify-content: center;
  background-repeat: no-repeat;
  object-fit: fill;
  
  `;
const Wrapper = styled.div`
  width: 40%;
  padding: 20px;
  background-color: #fff;
  ${mobile({ width: "80%"})};

`;
const Form = styled.form`
  display: flex;
  flex-wrap: wrap;

`;
const Title = styled.h1`
  font-size: 24px;
  font-weight: 300;
`;
const Input = styled.input`
  flex: 1;
  min-width: 40%;
  margin: 20px 10px 0px 0px;
  padding: 10px;
`;
const Agreement = styled.span`
  font-size: 12px;
  margin: 20px 0px;
`;
const Button = styled.button`
  width: 40%;
  border: none;
  padding: 15px 20px;
  background-color: teal;
  color: white;
  cursor: pointer;  
`;


const Register = () => {
  return (
    <Container>
      <Wrapper>
        <Title>UTWÓRZ KONTO</Title>
        <Form>
          <Input placeholder="Imię" />
          <Input placeholder="Nazwisko" />
          <Input placeholder="Nazwa użytkownika" />
          <Input placeholder="E-mail" />
          <Input placeholder="Hasło" />
          <Input placeholder="Powtórz hasło" />
          <Agreement>Szczegółowe zasady dotyczące przetwarzania i ochrony danych osobowych Klientów zawiera Polityka Prywatności znajdująca się na stronie Sklepu</Agreement>
          <Button>UTWÓRZ KONTO</Button>
        </Form>
      </Wrapper>

    </Container>
  );
};

export default Register;