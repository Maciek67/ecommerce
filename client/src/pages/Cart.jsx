import { Add, Remove } from "@material-ui/icons";
import styled from "styled-components";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import { mobile, tablet } from "../responsive";
import { useSelector } from "react-redux";
import StripeCheckout from "react-stripe-checkout";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom"
import { userRequest } from "../requestMethods";

const KEY = process.env.REACT_APP_STRIPE;

const Container = styled.div``;
const Wrapper = styled.div`
  padding: 20px;
  ${mobile({ padding: "10px"})};
  ${tablet({ padding: "15px", width: "95%"})};
`;
const Title = styled.h1`
  font-weight: 300;
  text-align: center;
`;
const Top = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 20px;
`;

const TopButton = styled.button`
  padding: 10px;
  font-weight: 600;
  cursor: pointer;
  border: ${props=>props.type === "filled" && "none"};
  background-color: ${props=>props.type === "filled" ?  "black" : "transparent"};
  color: ${props=>props.type === "filled" && "white"};
`;

const TopTexts = styled.div`
  ${mobile({ display: "none"})};
  ${tablet({ display: "none"})};
  `;

const TopText = styled.span`
  text-decoration: underline;
  cursor: pointer;
  margin: 0px 10px;
`;

const Bottom = styled.div`
  display: flex;
  justify-content: space-between;
  ${mobile({ flexDirection: "column"})};
`;
const Info = styled.div`
  flex: 3;
`;

const Product = styled.div`
  display: flex;
  justify-content:space-between;
  ${mobile({ flexDirection: "column"})};
  ${tablet({ flexDirection: "column"})};
`;
const ProductDetail = styled.div`
  flex: 2;
  display: flex;
`;
const Image = styled.img`
  width: 200px;
`;
const Details = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;

`;
const ProductName = styled.span``;
const ProductId = styled.span``;
const ProductColor = styled.div`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  background-color: ${props=>props.color};
`;
const ProductSize = styled.span``;
const PriceDetail  = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;


const ProductAmountContainer = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 20px;
`;

const ProductAmount = styled.div`
  font-size: 24px;
  margin: 5px;
  ${mobile({ margin: "5px 15px"})};
  ${tablet({ margin: "10px 20px"})};
`;

const ProductPrice = styled.div`
font-size: 30px;
font-weight: 200;
${mobile({ marginBottom: "20px"})};
${tablet({ marginBottom: "30px"})};
`;

const Hr =styled.hr`
  background-color: #eee;
  border: none;
  height: 1px;
`;



const Summary = styled.div`
  flex: 1;
  border: 0.5px solid lightgray;
  border-radius: 10px;
  padding: 20px;
  height: 50vh;
`;

const SummaryTitle = styled.h1`
  font-weight: 200;

`;
const SummaryItem = styled.div`
  margin: 30px 0px;
  display: flex;
  justify-content: space-between;
  font-weight: ${props=>props.type === "total" &&"500"};
  font-size: ${props=>props.type === "total" && "24px"};
  
`;
const SummaryItemText = styled.span``;
const SummaryItemPrice = styled.span``;
const SummaryButton = styled.button`
  width:100%;
  padding:10px;
  background-color: black;
  color: white;
  font-weight: 600;
`;


const Cart = () => {

  const cart = useSelector(state=>state.cart);
  const [stripeToken, setStripeToken] = useState(null);
  const navigate = useNavigate();

  const onToken = (token) => {
    setStripeToken(token);

  };

useEffect(()=> {
  const makeRequest = async ()=> {
    try{
      const res = await userRequest.post("/checkout/payment", {
        tokenId: stripeToken.id,
        amount: cart.total * 100,
        
      });
      navigate.push("/success",{
        stripeData: res.data,
        products: cart,
      });
    }catch{}
  };
  stripeToken && cart.total >= 1 && makeRequest();
}, [stripeToken, cart, cart.total, navigate]);


  return (
    <Container>
          <Navbar />
            <Wrapper>
              <Title>TWÓJ KOSZYK</Title>
              <Top>
                <TopButton>KONTYNUUJ ZAKUPY</TopButton>
                <TopTexts>
                  <TopText>Koszyk z zakupami(2)</TopText>
                  <TopText>Lista życzeń(0)</TopText>
                </TopTexts>
                <TopButton type="filled">SPRAWDŹ KOSZYK</TopButton>
              </Top>
              <Bottom>
                <Info>

                  {cart.products.map(product=>(
                  <Product>
                    <ProductDetail>
                      <Image src={product.img} />
                      <Details>
                        <ProductName><b>{product.title}
                          </b> </ProductName>
                          <ProductId><b>ID:</b> {product._id}</ProductId>
                        <ProductColor color={product.color}/>
                        <ProductSize ><b>Długość:</b> {product.size}</ProductSize>
                      </Details>
                    </ProductDetail>
                    <PriceDetail>
                      <ProductAmountContainer>
                        <Add />
                        <ProductAmount>{product.quantity}</ProductAmount>
                        <Remove />
                      </ProductAmountContainer>
                      <ProductPrice>PLN {product.price*product.quantity}</ProductPrice>
                    </PriceDetail>
                  </Product>
                  ))}
                  <Hr />
                </Info>
                <Summary>
                  <SummaryTitle>PODSUMOWANIE ZAMÓWIENIA</SummaryTitle>
                  <SummaryItem>
                    <SummaryItemText>Suma</SummaryItemText>
                    <SummaryItemPrice>PLN {cart.total}</SummaryItemPrice>
                  </SummaryItem>
                  <SummaryItem>
                    <SummaryItemText>Opłata za wysyłkę</SummaryItemText>
                    <SummaryItemPrice>PLN 10</SummaryItemPrice>
                  </SummaryItem>
                  <SummaryItem>
                    <SummaryItemText>Rabat</SummaryItemText>
                    <SummaryItemPrice>PLN -10</SummaryItemPrice>
                    </SummaryItem>
                    <SummaryItem type="total">
                    <SummaryItemText >Do zapłaty</SummaryItemText>
                    <SummaryItemPrice>PLN {cart.total}</SummaryItemPrice>
                  </SummaryItem>
                  <StripeCheckout 
        name="Sklep narciarski"
        image="https://cdn.pixabay.com/photo/2014/03/24/17/08/skiing-295156_960_720.png"
        billingAddress
        shippingAddress
        description={`Do zapłaty: ${cart.total}PLN`}
        amount={cart.total * 100}
        token={onToken}
        stripKey={KEY}
      ><SummaryButton>
        Zapłać 
      </SummaryButton>
      </StripeCheckout>
                </Summary>
              </Bottom>
            </Wrapper>
          <Footer />
    </Container>
  );
};

export default Cart;