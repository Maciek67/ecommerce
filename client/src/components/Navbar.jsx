import React from 'react';

import styled from 'styled-components';

import { Search, ShoppingCartOutlined } from '@material-ui/icons';
import { Badge } from '@material-ui/core';
import { mobile, tablet } from '../responsive';
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";



const Container = styled.div`
  height: 60px;
`;

const Wrapper = styled.div`
padding: 10px 20px;
display: flex;
align-items: center;
justify-content: space-between;
${mobile({ padding: "10px 0px"})}
`;

const Left = styled.div`
flex: 1;
display: flex;
align-items: center;
${mobile({ flex: "0.5"})}
`;
const Language = styled.span`
font-size: 16px;
cursor: pointer;
margin-top: 8px;
${mobile({ display: "none"})};
`;
const SearchContainer = styled.div`
border: 0.5px solid gray;
display: flex;
align-items: center;
margin-left: 25px;
padding: 5px; 
${mobile({ marginLeft: "10px"})};
`;

const Input = styled.input`
border: none;
${mobile({ width: "40px"})}
`;
const Center = styled.div`
flex: 1;
text-align: center;

`;
const Logo = styled.h1`
font-weight: bold;
${mobile({ fontSize: "20px"})};
${tablet({fontSize: "22px"})};
`;
const Right = styled.div`
flex: 1;
display: flex;
align-items: center;
justify-content: flex-end;
${mobile({justifyContent:"center",flex: "2"})};
  ${tablet({justifyContent:"center",flex: "1.5"})};
 `;
const MenuItem = styled.div`
font-size: 16px;
cursor: pointer;
margin-left: 25px;
${mobile({ fontSize: "10px", marginLeft: "10px"})};
${tablet({ fontSize: "16px"})};
`;



const Navbar = ()=> {
  const quantity = useSelector(state => state.cart.quantity)
  

  return (
    <Container>
      <Wrapper>
        <Left>
          <Language>PL</Language>
          <SearchContainer>
            <Input placeholder='Szukaj ...'/>
            <Search style={{color: "gray", fontSize: 18}}/>
          </SearchContainer>
        </Left>
        <Center><Logo>Sklep narciarski</Logo></Center>
        <Right>
          <Link to="/register">
          <MenuItem>REJESTRACJA</MenuItem>
          </Link>
          <Link to="/login">
          <MenuItem>ZALOGUJ SIĘ</MenuItem>
          </Link>
          <Link to="/cart">
          <MenuItem>
          <Badge badgeContent={quantity} color="primary">
            <ShoppingCartOutlined />
          </Badge>
          </MenuItem>
          </Link>
        </Right>
      </Wrapper>
    </Container>
  );
};

export default Navbar;
