// import { SettingsSystemDaydreamTwoTone } from '@material-ui/icons';
import { ArrowLeftOutlined, ArrowRightOutlined } from '@material-ui/icons';
import React, { useState } from 'react';
import styled from 'styled-components';
// import img_1  from '../assets/img_1.jpg';
// import img_2  from '../assets/img_2.jpg';
// import img_3  from '../assets/img_3.jpg';
import { sliderItems } from '../data';
import { mobile } from '../responsive';


const Container = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
`;

const Arrow = styled.div`
  width: 50px;
  height: 50px;
  background-color: #fff7f7;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 0;
  bottom: 0;
  left: ${props => props.direction === "left" && "10px"};
  right: ${props => props.direction === "right" && "10px"};
  margin: auto;
  cursor: pointer;
  opacity: 0.5;
  z-index: 2;
`;


const Wrapper = styled.div`
  height: 100%;
  width: 100vw;
  display: flex;
  transition: all 1.5s ease;
  transform: translateX(${(props) => props.slideIndex * -100}vw);
`;

const Slide = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: center;
  position: relative; 
`;

const ImgContainer = styled.div`
  display: flex;
  width: 300vw;
  height: 100vh;
  top: 0;
  left: 0;
`; 

const Img = styled.img`
  object-fit: cover;
  position: absolute;
  width: 100vw;
  height: 100vh;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const InfoContainer = styled.div`
  padding: 50px;
  position: absolute;
  top: 150px;
  left: 0;
`;

const Title = styled.h1`
  font-size: 70px;
  ${mobile({ fontSize: "50px"})};
`;

const Desc = styled.p`
  margin: 50px 0px;
  font-size: 20px;
  font-weight: 500;
  letter-spacing: 3px;
`;

const Button = styled.button`
  padding: 10px;
  font-size: 20px;
  background-color: transparent;
  cursor: pointer;
`;

const Slider = () => {
  const [slideIndex, setSlideIndex] = useState(0);
  const handleClick = (direction) => { 
    if(direction === "left") {
      setSlideIndex(slideIndex > 0 ? slideIndex-1 : 2)
    } else {
      setSlideIndex(slideIndex < 2 ? slideIndex+1 : 0)
    }
  }
  return (
    <Container>
      <Arrow direction="left" onClick={()=> handleClick("left")}>
        <ArrowLeftOutlined />
      </Arrow>
        <Wrapper slideIndex={slideIndex}>
          {sliderItems.map((item) => (
          <Slide key={item.id}>
            <ImgContainer>
              <Img src={item.img} alt="narciarz"/>
            </ImgContainer>
            <InfoContainer>
              <Title>{item.title}</Title>
              <Desc>{item.desc}</Desc>
              <Button>POKAŻ TERAZ</Button>
            </InfoContainer>
          </Slide>
          ))}
        </Wrapper>
      <Arrow direction="right" onClick={()=> handleClick("right")}>
        <ArrowRightOutlined />
      </Arrow>
    </Container>
  );
};

export default Slider;