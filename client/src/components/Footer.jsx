import { MailOutline, Phone, Room } from "@material-ui/icons";
import { Facebook, Instagram, Pinterest, Twitter, WhatsApp } from "@material-ui/icons";
import styled from "styled-components";
import { mobile, tablet } from "../responsive";

const Container = styled.div`
  display: flex;
  ${mobile({ flexDirection: "column"})};
`;
const Left = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  padding: 20px;
`;

const Logo = styled.h1`

`;
const Desc = styled.p`
  margin: 20px 0px;
`;
const SocialContainer = styled.div`
  display: flex;
`;
const SocialIcon = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  color: white;
  background-color: #${props=> props.color};
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 20px;
`;


const Center = styled.div`
  flex: 1;
  padding: 20px;
  ${mobile({ display: "none"})};
  ${tablet({ display: "none"})};
  

`;
const Title = styled.h3`
  margin-bottom: 30px;
`;
const List = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
  display: flex;
  flex-wrap: wrap;
`;

const ListItem = styled.li`
  width: 50%;
  margin-bottom: 10px;
`;
  

const Right = styled.div`
  flex: 1;
  padding: 20px;
  ${mobile({ backgroundColor: "#fff8f8"})};
`;

const ContactItem = styled.div`
  margin-bottom: 20px;
  display: flex;
  align-items: center;
`;

const Payment = styled.img`
  width: 50%;
`;


const Footer = () => {
  return (
    <Container>
      <Left>
        <Logo>Sklep narciarski</Logo>
        <Desc>
          Wszystkich miłośników wycieczek narciarskich zapraszamy do zapoznania się z ofertą naszego sklepu.
        </Desc>
        <SocialContainer>
          <SocialIcon color="3B5999">
            <Facebook/> 
          </SocialIcon>
          <SocialIcon color="E4405F">
            <Instagram /> 
          </SocialIcon>
          <SocialIcon color="55ACEE">
            <Twitter /> 
          </SocialIcon>
          <SocialIcon color="E60023">
            <Pinterest />
          </SocialIcon>
          <SocialIcon color="008080">
            <WhatsApp /> 
          </SocialIcon>  
        </SocialContainer>
      </Left>
      <Center>
        <Title>Odnośniki :</Title>
        <List>
          <ListItem>Home</ListItem>
          <ListItem>Karta</ListItem>
          <ListItem>Kobiety</ListItem>
          <ListItem>Mężczyźni</ListItem>
          <ListItem>Juniorzy</ListItem>
          <ListItem>Moje konto</ListItem>
          <ListItem>Śledzenie zamówienia</ListItem>
          <ListItem>Życzenia</ListItem>
          <ListItem>Regulamin</ListItem>
        </List>

      </Center>
      <Right>
        <Title>Kontakt</Title>
        <ContactItem>
          <Room style={{marginRight: "10px"}}/>Rynek Główny, 30-001 Kraków
        </ContactItem>
        <ContactItem>
          <Phone style={{marginRight: "10px"}}/> +48 12 245 888 999</ContactItem>
        <ContactItem>
          <MailOutline style={{marginRight: "10px"}}/>sklepnarciarski@wpp.pl</ContactItem>
        <Payment src="https://i.ibb.co/Qfvn4z6/payment.png" />
      </Right>
    </Container>

  );
};

export default Footer;