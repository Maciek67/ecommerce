import axios from "axios";

const BASE_URL = "http://localhost:5000/api/";

const TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxZjdjODAzZTc5MzAxMDdlN2Q4ZTc5YSIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY0Mzk5OTA2MiwiZXhwIjoxNjQ0MjU4MjYyfQ.Jt-OD0ipKltzFIN5FRWibTs52GMi70Rs1LUhlET7ioE";

export const publicRequest = axios.create({
  baseURL: BASE_URL,
});


export const userRequest = axios.create({
  baseURL: BASE_URL,
  header:{token: `Bearer ${TOKEN}`},
  });