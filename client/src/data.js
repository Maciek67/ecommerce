export const sliderItems = [
{
  id: 1,
  img: "https://cdn.pixabay.com/photo/2014/10/22/18/04/man-498473__480.jpg",
  title: "ZIMOWA WYPRZEDAŻ",
  desc: "Obniżka 30% dla nowo zarejestrowanych klientów !",
  
},
{
  id: 2,
  img: "https://cdn.pixabay.com/photo/2016/04/30/20/29/ski-tour-1363754__340.jpg",
  title: "POSEZONOWA WYPRZEDAŻ",
  desc: "Obniżka 40% dla nowo zarejestrowanych klientów !",
  
},
{
  id: 3,
  img: "https://cdn.pixabay.com/photo/2021/09/26/11/49/skiing-6657414__340.jpg",
  title: "TOTALNA WYPRZEDAŻ",
  desc: "Obniżka 20% dla nowo zarejestrowanych klientów !",
  
},

];

export const categories = [
  {
  id: 1,
  img: "https://cdn.pixabay.com/photo/2017/01/24/23/06/skiing-2006663_960_720.jpg",
  title: "KOBIETY",
  category: "kobiety",
  },
  {
    id: 2,
    img: "https://cdn.pixabay.com/photo/2017/03/04/14/15/mountain-2116142_960_720.jpg",
    title: "MĘŻCZYŹNI",
    category: "mężczyźni",
  },
  {
    id: 3,
    img: "https://cdn.pixabay.com/photo/2016/01/17/01/36/ski-1144345__340.jpg",
    title: "JUNIORZY",
    category: "juniorzy",
  }
];

export const popularProducts = [
  {
    id: 1,
    img: "https://ireland.apollo.olxcdn.com/v1/files/0vxm669wqkjr2-PL/image;s=644x461",
    
  },
  {
    id: 2,
    img: "https://ireland.apollo.olxcdn.com/v1/files/xjve0pzrcumk-PL/image;s=644x461",
    
  },
  {
    id: 3,
    img: "https://ireland.apollo.olxcdn.com/v1/files/uddvesg2xxrm-PL/image;s=644x461",
    
  },
  {
    id: 4,
    img: "https://ireland.apollo.olxcdn.com/v1/files/tk2pd52icld53-PL/image;s=644x461",
    
  },
  {
    id: 5,
    img: "https://ireland.apollo.olxcdn.com/v1/files/wbxud5sff9jw2-PL/image;s=644x461",
    
  },
  {
    id: 6,
    img: "https://ireland.apollo.olxcdn.com/v1/files/nwx7wqm884jv3-PL/image;s=644x461",
    
  },
  {
    id: 7,
    img: "https://ireland.apollo.olxcdn.com/v1/files/55nq3uryxi3z-PL/image;s=644x461",
    
  },
  {
    id: 8,
    img: "https://ireland.apollo.olxcdn.com/v1/files/ameqpx5x94n-PL/image;s=644x461",
    category: "juniorzy",
  },
  {
    id: 9,
    img: "https://ireland.apollo.olxcdn.com/v1/files/tepgxli0e4oe1-PL/image;s=644x461",
    
  },
  {
    id: 10,
    img: "https://ireland.apollo.olxcdn.com/v1/files/vi12tth7lf4q2-PL/image;s=644x461",
    
  },
]
