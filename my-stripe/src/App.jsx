import { Routes, Route } from 'react-router-dom';
import Success from './Success';
import Pay from './Pay';

const App = () => {
  return (
    
      <Routes>
        <Route path="/pay" element={<Pay />} >
          
        </Route>
        <Route path="/success" element={<Success />}>
          
        </Route>
      </Routes>
    
  );
};

export default App;
