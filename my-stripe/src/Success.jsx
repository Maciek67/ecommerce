

import React from 'react';

const Success = () => {

  return (
    <div style={{
      height: "100vh",
      width: "100vw",
      display: "flex",
      flexDirection: 'column',
      alignItems: "center",
      justifyContent: "center",
      fontFamily: "Roboto",
      backgroundImage: "url(https://cdn.pixabay.com/photo/2020/03/26/09/01/snow-4969647__340.jpg)",
      backgroundRepeat: "no-repeat",
  backgroundSize: "cover",

    }}
    >
      <h1 style={{
        width: "500px",
        height: "100px",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#0b0c54",
        color: "white",
        borderRadius: "5px",
        hightText: "100px",
        }}
        >Gratulacje, dokonałeś zakupu!</h1>
      <p style={{
        fontSize: "20px",
        color: "#ffffff",
      }}
      >
        Twoje zamówienie jest przygotowywane do wysyłki.
      </p>
    </div>
  )
};

export default Success;