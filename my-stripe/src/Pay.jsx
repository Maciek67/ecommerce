import React from 'react';

import StripeCheckout from 'react-stripe-checkout';
import { useState, useEffect } from 'react';
const  axios = require('axios').default;


const PUBLIC_KEY = "pk_test_51KOMkOAJJSJcII9iGaWK8gXoDIqg18r4opEVY0FYdqcesQQKqHrRgQpRXBw6MrYRIqopfqa0iEKP05Clg2UiQ9nX00hF5W2yzK";

const Pay = () => {

  const [stripeToken, setStripeToken] = useState(null);

  const onToken = (token) => {
    setStripeToken(token);
  };

  useEffect(()=> {
    const makeRequest = async () => {
      try {
        const res = await axios.post("http://localhost:5000/api/checkout/payment",
        {
          tokenId: stripeToken.id,
          amount: 2000,
        }
        );
        console.log(res.data);
      } catch(err) {
        console.log(err)
      }
    };
    stripeToken && makeRequest();
  }, [stripeToken]);

  return (
    <div style={{
      height: "100vh",
      width: "100vw",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      justifyItems: "center",
      backgroundImage: "URL(https://cdn.pixabay.com/photo/2018/08/29/11/05/snowflake-background-3639667_960_720.jpg)" ,
      backgroundSize: "cover",
    }}
    ><StripeCheckout 
        name="Sklep narciarski"
        image="https://cdn.pixabay.com/photo/2014/03/24/17/08/skiing-295156_960_720.png"
        billingAddress
        shippingAddress
        description="Do zapłaty: 20PLN"
        amount={2000}
        token={onToken}
        stripKey={PUBLIC_KEY}
      ><button style={{
        border: "none",
        width: 120,
        borderRadius: 5,
        padding: "20px",
        backgroundColor: "black",
        color: "white",
        fontWeight: "600",
        cursor: "pointer",
      }}
      >
        Zapłać 
      </button>
      </StripeCheckout>
    </div>
  )

};

export default Pay;